# Networking #

Progetto di esempio per spiegare i concetti di comunicazione di rete, indipendenza delle interfacce e comunicazione bidirezionale.

Le tecnologie utilizzate sono

- Socket

- RMI

Il progetto è strutturato in tre package, ciascuno con un progetto indipendente dagli altri.

## it.polimi.networking ##

Progetto base, per illustrare i principi delle tecnologie.

Contiene solo il server e due client per testare il server.

Pattern Command abbozzato.

## it.polimi.networking2 ##

Client un po' più sofisticato, consente la scelta dell'interfaccia di rete da utilizzare.

Implementato tramite il pattern Factory.

## it.polimi.networking3 ##

Esempio completo: implementa la comunicazione bidirezionale.

Il server può invocare il client per inviare delle notifiche.