package it.polimi.networking.client.test;

import it.polimi.networking.server.rmi.RemoteOven;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class TestRMI {
	public static void main(String[] args) throws RemoteException, NotBoundException {
		String name = "Oven";
        Registry registry = LocateRegistry.getRegistry(2020);
        RemoteOven oven = (RemoteOven) registry.lookup(name);
        oven.turn(true);
        System.out.println("Now the oven is turned on? "+oven.isOn());
	}
}
