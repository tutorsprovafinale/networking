package it.polimi.networking.client.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestSocket {

	public static final void main(String[] args) throws UnknownHostException, IOException{
		Socket s = new Socket("127.0.0.1", 8888);
		PrintWriter pw = new  PrintWriter(s.getOutputStream(), true);
		BufferedReader br = new BufferedReader(
				new InputStreamReader(s.getInputStream()));
		pw.println("object=oven&action=turn&value=on");
		String read = br.readLine();
		System.out.println(read);
		pw.close();
		br.close();
		s.close();
		
	}
}
