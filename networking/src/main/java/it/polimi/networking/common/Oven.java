package it.polimi.networking.common;

import java.rmi.RemoteException;

import it.polimi.networking.server.rmi.RemoteOven;

public class Oven implements RemoteOven {

	private boolean on;
	private double temperature;	
	
	public boolean isOn() throws RemoteException{
		return on;
	}
	
	public double getTemperature() {
		return temperature;
	}
	
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	public boolean turn(boolean value) throws RemoteException {
		this.on = value;
		return this.on;
	}

}
