package it.polimi.networking.common;

import java.rmi.RemoteException;

public class House {
	private static House instance;
	
	private House(){
		oven = new Oven();
		oven.setTemperature(0);
		try {
			oven.turn(false);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static House getInstance(){
		if(instance == null) instance = new House();
		return instance;
	}
	
	private Oven oven;

	public Oven getOven() {
		return oven;
	}

	
}
