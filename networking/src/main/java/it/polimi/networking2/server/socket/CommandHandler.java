package it.polimi.networking2.server.socket;

import it.polimi.networking2.common.House;

import java.rmi.RemoteException;
import java.util.Map;

public class CommandHandler {

	private CommandHandler(){
		
	}
	
	public static String handleCommand(Map<String, String> param) throws RemoteException{
		String object = param.get("object");
		String action = param.get("action");
		String toRet = "Command unknown";
		if(object.equals("oven")){
			if(action.equals("turn")){
				String value = param.get("value");
				if(value.equals("on")){
					House.getInstance().getOven().turn(true);
					toRet = "Oven turned on";
				} else {
					House.getInstance().getOven().turn(false);
					toRet = "Oven turned off";
				}
			} else if (action.equalsIgnoreCase("status")){
				toRet = ""+House.getInstance().getOven().isOn();
			}
		}
		return toRet;
	}
}
