package it.polimi.networking2.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteOven extends Remote {
	public boolean turn(boolean value) throws RemoteException;
	public boolean isOn() throws RemoteException;
}
