package it.polimi.networking2.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

public class SocketInterface implements NetworkInterface {

	private Socket s;
	private PrintWriter pw;
	private BufferedReader br; 
	
	public SocketInterface() {
		
	} 
	
	public boolean connect() throws IOException {
		try {
			s = new Socket("127.0.0.1", 8888);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		try {
			pw = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {			
			e.printStackTrace();
			s.close();
			return false;
		}
		try {
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {			
			e.printStackTrace();
			pw.close();
			s.close();
			return false;
		}
		return true;
	}

	public boolean close() {
		try{
			br.close();
			pw.close();
			s.close();
			return true;
		} catch(IOException ex){
			return false;
		}
	}

	public void turnOvenOn() throws RemoteException {
		pw.println("object=oven&action=turn&value=on");
		try {
			br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void turnOvenOff() throws RemoteException {
		pw.println("object=oven&action=turn&value=off");
		try {
			br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public boolean getOvenStatus() throws RemoteException {
		pw.println("object=oven&action=status");
		boolean toRet = false; 
		try {
			String read = br.readLine();	
			toRet = Boolean.parseBoolean(read);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return toRet;
	}

}
