package it.polimi.networking2.client;

import java.io.IOException;
import java.rmi.RemoteException;

public interface NetworkInterface {
	boolean connect() throws IOException;
	boolean close() throws IOException;
	
	void turnOvenOn() throws RemoteException;
	void turnOvenOff() throws RemoteException;;
	boolean getOvenStatus() throws RemoteException;;
}
