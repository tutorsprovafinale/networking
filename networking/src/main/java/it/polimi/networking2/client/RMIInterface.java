package it.polimi.networking2.client;

import it.polimi.networking2.server.rmi.RemoteOven;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIInterface implements NetworkInterface {

	private RemoteOven oven;
	
	public boolean connect() {
		String name = "Oven";
        Registry registry;
		try {
			registry = LocateRegistry.getRegistry(2020);
		} catch (RemoteException e) {			
			e.printStackTrace();
			return false;
		}
        try {
			oven = (RemoteOven) registry.lookup(name);
		} catch (AccessException e) {
			e.printStackTrace();
			return false;
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (NotBoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean close() {
		return true;
	}

	public void turnOvenOn() throws RemoteException {
		oven.turn(true);
		
	}

	public void turnOvenOff() throws RemoteException{
		oven.turn(false);
		
	}

	public boolean getOvenStatus() throws RemoteException{
		return oven.isOn();
	}

}
