package it.polimi.networking3.client.test;

import it.polimi.networking3.server.rmi.RemoteOven;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author Riccardo Cipolleschi
 * Test client to check if the server is correclty exposing RMI.
 */
public class TestRMI {
	public static void main(String[] args) throws RemoteException, NotBoundException {
		String name = "Oven";
        Registry registry = LocateRegistry.getRegistry(2020);
        RemoteOven oven = (RemoteOven) registry.lookup(name);
        oven.turn(true);
        System.out.println("Now the oven is turned on? "+oven.isOn());
	}
}
