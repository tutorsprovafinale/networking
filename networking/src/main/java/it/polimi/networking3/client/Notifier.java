package it.polimi.networking3.client;

import java.rmi.RemoteException;

import it.polimi.networking3.server.rmi.RemoteNotifier;

/**
 * @author Riccardo Cipolleschi
 * Method that implements the RemoteNotifier interface.
 */
public class Notifier implements RemoteNotifier {

	/* (non-Javadoc)
	 * @see it.polimi.networking3.server.rmi.RemoteNotifier#notifyMessage(java.lang.String, java.lang.String)
	 */
	public void notifyMessage(String message, String source)
			throws RemoteException {
		System.out.println(source.toUpperCase()+") "+message);

	}

}
