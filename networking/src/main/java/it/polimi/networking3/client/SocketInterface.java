package it.polimi.networking3.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

/**
 * @author Riccardo Cipolleschi
 * Method that implements the NetworkInterface.
 * It allows the client to communicate with the server 
 * over a socket connection.
 */
public class SocketInterface implements NetworkInterface {

	private Socket s;
	private PrintWriter pw;
	private BufferedReader br; 
	
	/**
	 * Default constructor.
	 */
	public SocketInterface() {
		
	} 
	
	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#connect()
	 */
	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#connect()
	 */
	public boolean connect() throws IOException {
		try {
			//Create a socket and connect it to the server.
			s = new Socket("127.0.0.1", 8888);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		try {
			//Get the output stream to write to the server.
			pw = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {			
			e.printStackTrace();
			s.close();
			return false;
		}
		try {
			//Get the input stream to read from the server.
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {			
			e.printStackTrace();
			pw.close();
			s.close();
			return false;
		}
		//Send its own address and port to the server so that it
		//can create a socket back to the client.
		pw.println("address=127.0.0.1&port=9898");
		System.out.println(br.readLine());
		return true;
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#close()
	 */
	public boolean close() {
		try{
			br.close();
			pw.close();
			s.close();
			return true;
		} catch(IOException ex){
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#turnOvenOn()
	 */
	public void turnOvenOn() throws RemoteException {
		pw.println("object=oven&action=turn&value=on");
		try {
			br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#turnOvenOff()
	 */
	public void turnOvenOff() throws RemoteException {
		pw.println("object=oven&action=turn&value=off");
		try {
			br.readLine();	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#getOvenStatus()
	 */
	public boolean getOvenStatus() throws RemoteException {
		pw.println("object=oven&action=status");
		boolean toRet = false; 
		try {
			String read = br.readLine();	
			toRet = Boolean.parseBoolean(read);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return toRet;
	}

}
