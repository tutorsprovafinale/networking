package it.polimi.networking3.client;

/**
 * @author Riccardo Cipolleschi
 * Factory method class. It creates the correct Network Interface.
 */
public class NetworkInterfaceFactory {

	private NetworkInterfaceFactory(){
		
	}
	
	/**
	 * Static method that creates the method interface.
	 * @param param <b>1</b> to create a Socket interface; <b>2</b> to create an RMI interface.
	 * @return the required NetworkInterface
	 */
	public static NetworkInterface getInterface(String param){
		if(param.equals("1")) return new SocketInterface();
		else return new RMIInterface();
	}
}
