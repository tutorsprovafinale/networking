package it.polimi.networking3.client;

import it.polimi.networking3.server.rmi.CallableClient;
import it.polimi.networking3.server.rmi.RemoteOven;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author Riccardo Cipolleschi
 * Class that implements the NetworkInterface.
 * It implements all the methods such that they can communicate
 * with the server throught the RMI protocol.
 */
public class RMIInterface implements NetworkInterface {

	private RemoteOven oven;
	
	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#connect()
	 */
	public boolean connect() {
		String name = "Oven";
		String clientName = "Client";
        Registry registry;
		try {
			//Get the Registry
			registry = LocateRegistry.getRegistry(2020);
		} catch (RemoteException e) {			
			e.printStackTrace();
			return false;
		}
        try {
        	//Get the oven reference
			oven = (RemoteOven) registry.lookup(name);
			//Register the client to the server.
			((CallableClient) registry.lookup(clientName)).setClientPort(3030);
		} catch (AccessException e) {
			e.printStackTrace();
			return false;
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		} catch (NotBoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#close()
	 */
	public boolean close() {
		return true;
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#turnOvenOn()
	 */
	public void turnOvenOn() throws RemoteException {
		oven.turn(true);
		
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#turnOvenOff()
	 */
	public void turnOvenOff() throws RemoteException{
		oven.turn(false);
		
	}

	/* (non-Javadoc)
	 * @see it.polimi.networking3.client.NetworkInterface#getOvenStatus()
	 */
	public boolean getOvenStatus() throws RemoteException{
		return oven.isOn();
	}

}
