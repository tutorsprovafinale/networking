package it.polimi.networking3.client;

import it.polimi.networking3.server.rmi.RemoteNotifier;
import it.polimi.networking3.server.socket.SocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Riccardo Cipolleschi
 * Class which is used to instantiate the client.
 * It asks to the user which network interface must be used.
 * This client starts a socket server and creates a RMI registry 
 * to be called by the server.
 */
public class Client {

	public static void main(String[] args) throws IOException, NotBoundException {
		//Creating and starting the socket server
		//For semplicity sake, the program reuse the socket server class
		//That means that it can accept more than one connection. 
		//This is not the recommended settings. 
		SocketServer ss = new SocketServer(9898, "127.0.0.1");
		Thread t = new Thread(ss);
		t.start();
		
		//Starting RMI registry
		Registry registry = null;
		String notName = "Notifier";
		
		       
        try {
            //Binding the notifier
            RemoteNotifier notifier = new Notifier();
            RemoteNotifier stub =
                (RemoteNotifier) UnicastRemoteObject.exportObject(notifier, 0);            
            registry = LocateRegistry.createRegistry(3030);            
            registry.bind(notName, stub);
            
            
        } catch (Exception e) {
            System.err.println("RMI exception:");
            e.printStackTrace();
            registry = null;
        }
		
		//Which network interface must be used?
		String read = "";
		while(!read.equals("1")  && !read.equals("2")){
			System.out.println("Scegli che interfaccia di rete usare:");
			System.out.println("1 - Socket");
			System.out.println("2 - RMI");
			read = readLine("\n");
			if(!read.equals("1")  && !read.equals("2"))
				System.out.println("Comando non riconosciuto!");
		}
		
		//Factory method to create the interface.
		NetworkInterface ni = NetworkInterfaceFactory.getInterface(read);
		//using the interface
		ni.connect();
		boolean isOn = ni.getOvenStatus();
		System.out.println("The oven is on?\n" +isOn);
		if(isOn)
			ni.turnOvenOff();
		else
			ni.turnOvenOn();
		System.out.println("Now the oven is on?\n"+ni.getOvenStatus());
		ni.close();
		
		//Loop to query/stop the client
		boolean end = false;
		while(!end){
			read = readLine("Press Q to exit");
			if(read.equals("Q"))
				end = true;
		}
		
		ss.endListening();
		t.interrupt();
		if(registry != null){
			registry.unbind(notName);
		}
		System.exit(0);
	}
	
	/**
	 * Helper method to read a line from the console when the 
	 * program is started in Eclipse
	 * @param format the formet string to be read
	 * @param args arguments for the format
	 * @return the read string
	 * @throws IOException if the program cannot access the stdin
	 */
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    System.out.print(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
	    return read;
	}
}
