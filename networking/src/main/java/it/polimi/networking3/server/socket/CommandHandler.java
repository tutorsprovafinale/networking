package it.polimi.networking3.server.socket;

import it.polimi.networking3.common.House;

import java.rmi.RemoteException;
import java.util.Map;

/**
 * @author Riccardo Cipolleschi
 * Lazy implementation of the Command pattern.
 * Do not use this but implement a hierarchical one if you like the pattern.
 */
public class CommandHandler{

	/**
	 * Default contructor
	 */
	private CommandHandler(){
		
	}
	
	/**
	 * Static method that handles the Command. 
	 * @param param the command, represented as a Map
	 * @return the computation result as a string
	 * @throws RemoteException is some RemoteObject cannot be used.
	 */
	public static String handleCommand(Map<String, String> param) throws RemoteException{
		//Get the target object and the action
		String object = param.get("object");
		String action = param.get("action");
		String toRet = "Command unknown";
		if(object.equals("oven")){
			//Handle the oven command
			if(action.equals("turn")){
				//Switching the oven on/off
				String value = param.get("value");
				if(value.equals("on")){
					House.getInstance().getOven().turn(true);
					toRet = "Oven turned on";
				} else {
					House.getInstance().getOven().turn(false);
					toRet = "Oven turned off";
				}
			} else if (action.equalsIgnoreCase("status")){				
				toRet = ""+House.getInstance().getOven().isOn();
			}
		} else if(object.equalsIgnoreCase("message")){
			//Printing a server message on the client console.
			if(action.equals("print")){
				String value = param.get("message");
				if(param.containsKey("source"))
					value=param.get("source").toUpperCase()+") "+value;
				System.out.println(value);
			}
		}
		return toRet;
	}
}
