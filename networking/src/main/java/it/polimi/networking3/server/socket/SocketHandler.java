package it.polimi.networking3.server.socket;

import it.polimi.networking3.common.House;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Riccardo Cipolleschi
 * Class that handle a single socket connection
 */
public class SocketHandler extends Thread{
	private Socket socket, toClientSocket;
	private PrintWriter out, toClientOut;
	private BufferedReader in, toClientIn;
	private boolean stop;
	private Timer timer;

	/**
	 * The socket passed by the server.
	 * @param socket on which the handler and the client can communicate
	 */
	public SocketHandler(Socket socket) {
		super();
		this.socket = socket;
		stop = false;				
	}

	public Socket getSocket() {
		return socket;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		try{
			//Get the input and output stream
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			String input, output;
			//While there is something to read
			while((input = in.readLine()) != null && !stop){
				//PArsing the UrlQuery protocol
				String[] splitted = input.split("&");
				Map<String, String> params = new HashMap<String, String>();
				for(String s : splitted){
					params.put(s.split("=")[0],s.split("=")[1]);
				}
				//If the message sent is the client reference
				//the socket handler open a second socket to write to the client
				//the client acts as a small server which accepts only one connection.
				if(params.containsKey("port")){					
					if(toClientSocket == null){
						//Check if no socket has already been created
						toClientSocket = new Socket(params.get("address"), Integer.parseInt(params.get("port")));
						toClientOut = new PrintWriter(toClientSocket.getOutputStream(), true);
						toClientIn = new BufferedReader(
								new InputStreamReader(toClientSocket.getInputStream()));
						//Start a timer to periodically send messages to the client.
						timer = new Timer();
						timer.scheduleAtFixedRate(new TimerTask() { 
							//Anonymous class which implements the TimerTask interface.
							
							@Override
							public void run() {
								try{
									toClientOut.println("source=server&message="+new Timestamp(System.currentTimeMillis())+" - The oven is "+(House.getInstance().getOven().isOn()?"on":"off")+"&object=message&action=print");
								}catch(RemoteException ex){
									ex.printStackTrace();
								}
							}
						}, 0, 5000);
						//Write a message back to the client.
						out.println("object=message&action=print&message=duplex connection established");
					} else {
						out.println("object=message&action=print&message=error: socket already instantiated");
					}
				} else {
					//A normal message has been sent to the SocketHandler
					//Use the Command Pattern to handle it.
					output = CommandHandler.handleCommand(params);
					out.println(output);
				}
				
			}
		
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}

	/**
	 * Method that cleans the resources used by the SocketHandler.
	 * @throws IOException if something cannot be closed.
	 */
	public void Close() throws IOException{
		stop = true;
		in.close();
		out.close();
		socket.close();
		if(timer != null)
			timer.cancel();
		if(toClientIn != null)
			toClientIn.close();
		if(toClientOut != null)
			toClientOut.close();
		if(toClientSocket != null)
			toClientSocket.close();
	}
	
	
}
