package it.polimi.networking3.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Riccardo Cipolleschi
 * Remote interface for the oven.
 * It should expose only the methods that 
 * are available to the clients
 */
public interface RemoteOven extends Remote {
	
	/**
	 * Method turn on/off the oven
	 * @param value the new oven status
	 * @return the current status of the oven.
	 * @throws RemoteException
	 */
	public boolean turn(boolean value) throws RemoteException;
	/**
	 * Method used to check the status of the oven
	 * @return the current status of the oven.
	 * @throws RemoteException
	 */
	public boolean isOn() throws RemoteException;
}
