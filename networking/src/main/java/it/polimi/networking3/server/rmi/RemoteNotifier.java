package it.polimi.networking3.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Riccardo Cipolleschi
 * Remote interface exposed by the clients. 
 * This allows the server to send messages to the clients.
 *
 */
public interface RemoteNotifier extends Remote {

	/**
	 * Method that receives a message from a caller
	 * @param message sent by the caller
	 * @param source who is sending the message
	 * @throws RemoteException
	 */
	void notifyMessage(String message, String source) throws RemoteException;
}
