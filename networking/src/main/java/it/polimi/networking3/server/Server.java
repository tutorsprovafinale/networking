package it.polimi.networking3.server;

import it.polimi.networking3.common.House;
import it.polimi.networking3.common.RemoteCallableClient;
import it.polimi.networking3.server.rmi.CallableClient;
import it.polimi.networking3.server.rmi.RemoteOven;
import it.polimi.networking3.server.socket.SocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Riccardo Cipolleschi
 * Executable class which spawns the Socket server and the
 * RMI registry.
 */
public class Server {
	public static void main(String[] args) throws IOException, NotBoundException{
		Registry registry = null;
		String name = "Oven";
		String clientName = "Client";  
		
		//Starting RMI registry		       
        try {
            //Creating the oven and its RemoteObject
            RemoteOven oven = House.getInstance().getOven();
            RemoteOven stub =
                (RemoteOven) UnicastRemoteObject.exportObject(oven, 0);            
            //Creating the callable client and its RemoteObject
            CallableClient client = new RemoteCallableClient();
            CallableClient clientStub = (CallableClient) UnicastRemoteObject.exportObject(client, 4040);
            //Creating the registry
            registry = LocateRegistry.createRegistry(2020);            
            //Binding the remote objects
            registry.bind(name, stub);
            registry.bind(clientName, clientStub);
                        
        } catch (Exception e) {
            System.err.println("RMI exception:");
            e.printStackTrace();
            registry = null;
        }
		
		//Starting socket server
		SocketServer server = new SocketServer();
		System.out.println("Starting the server...");
		//Starting a different thread not to block the Server CLI
		Thread t = new Thread(server);
		t.start();
		System.out.println("Server started. Status: "+server.getStatus()+". Port: "+server.getPort());
		boolean finish = false;
		//Loop to query the server status/stop the server.
		while(!finish){
			String read = readLine("Press S to get the server status;\nPress Q to exit\n");
			if(read.equals("Q")){
				finish = true;
			}
			if(read.equals("S")){
				System.out.println("Server status: "+server.getStatus());
			}
		}
		
		//Releasing the server resources.
		t.interrupt();
		server.endListening();
		if(registry != null)
			registry.unbind(name);
		
		System.exit(0);
	}
	
	/**
	 * Helper method to read a line from the console when the 
	 * program is started in Eclipse
	 * @param format the formet string to be read
	 * @param args arguments for the format
	 * @return the read string
	 * @throws IOException if the program cannot access the stdin
	 */
	private static String readLine(String format, Object... args) throws IOException {
	    if (System.console() != null) {
	        return System.console().readLine(format, args);
	    }
	    System.out.print(String.format(format, args));
	    
	    BufferedReader br = null;
	    InputStreamReader isr = null;
	    String read = null;
	    
	    isr = new InputStreamReader(System.in);
	    br = new BufferedReader(isr);
	    read = br.readLine();
	    
	    return read;
	}
}
