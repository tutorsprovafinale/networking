package it.polimi.networking3.common;

import java.rmi.RemoteException;

import it.polimi.networking3.server.rmi.RemoteOven;

/**
 * @author Riccardo Cipolleschi
 * Class which implements the RemoteOven interface.
 * This class has some methods that are not exposed to the clients.
 */
public class Oven implements RemoteOven {

	private boolean on;
	private double temperature;	
	
	/* (non-Javadoc)
	 * @see it.polimi.networking3.server.rmi.RemoteOven#isOn()
	 */
	public boolean isOn() throws RemoteException{
		return on;
	}
	
	public double getTemperature() {
		return temperature;
	}
	
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	/* (non-Javadoc)
	 * @see it.polimi.networking3.server.rmi.RemoteOven#turn(boolean)
	 */
	public boolean turn(boolean value) throws RemoteException {
		this.on = value;
		return this.on;
	}

}
