package it.polimi.networking3.common;

import it.polimi.networking3.server.rmi.CallableClient;
import it.polimi.networking3.server.rmi.RemoteNotifier;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Timestamp;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Riccardo Cipolleschi
 * Class which implements the CallableClient interface.
 * It allows the server to call the remote client.
 */
public class RemoteCallableClient implements CallableClient {

	private Timer t;
	
	/* (non-Javadoc)
	 * @see it.polimi.networking3.server.rmi.CallableClient#setClientPort(int)
	 */
	public void setClientPort(int port) throws RemoteException {
		Registry registry;
		try {
			//Locate the registry and get the remote notifier
			registry = LocateRegistry.getRegistry(port);
			RemoteNotifier rn = (RemoteNotifier)registry.lookup("Notifier");
			if(t==null)
				t = new Timer();
			//Schedule the timer task
			t.scheduleAtFixedRate(new IndependentNotifier(rn), 200, 5000);
		} catch (Exception ex){
			ex.printStackTrace();
		}

	}
	
	/**
	 * @author Riccardo Cipolleschi
	 * Inner class which represent the timer task used to sent
	 * the messages to the clients.
	 */
	private class IndependentNotifier extends TimerTask{
		private RemoteNotifier rn;
		
		/**
		 * Field constuctor.		  
		 * @param rn Remote Notifier used to send the messages to the client
		 */
		public IndependentNotifier(RemoteNotifier rn) {
			this.rn = rn;
		}

		/* (non-Javadoc)
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			try{
				String msg = new Timestamp(System.currentTimeMillis())+" - The oven is "+
					(House.getInstance().getOven().isOn()?"on":"off");
			
				rn.notifyMessage(msg, "server");
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}
}
