package it.polimi.networking3.common;

import java.rmi.RemoteException;

/**
 * @author Riccardo Cipolleschi
 * Singleton class which represents the house controlled 
 * by the system
 */
public class House {
	//Singleton
	private static House instance;
	
	
	private House(){
		oven = new Oven();
		oven.setTemperature(0);
		try {
			oven.turn(false);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Singleton method which instantates the house if it is null
	 * <b>This is not thread safe!</b>
	 * @return the House instance.
	 */
	public static House getInstance(){
		if(instance == null) instance = new House();
		return instance;
	}
	
	private Oven oven;

	public Oven getOven() {
		return oven;
	}

	
}
